!!! note
    Some items below are collapsed, just click on them to expand.

**Talk:** Introduction of the LKCAMP group

### (1) Where/How to make questions: establishing communication channels

#### Mailing lists

??? note "Why Mailing lists?"
    Linux development is completely centered around Mailing lists, and this is completely unlikely to change any time soon.  Hence, it is extremely important that each and everyone of us become fully capable of interacting and asynchronous discussing topics via email.

    The quickest way to get screamed at in a kernel related mailing list is to ignore general mailing list etiquette.  Email etiquette is all about being considerate of your reader so, before ever submitting a message to a mailing list, be it lkcamp, kml or any list, make sure you understand the rules of that community.

    ??? note "Common mistakes with dealing with Mailing lists"
        * Doing Top-posting instead of Bottom-posting: If you don’t know what Top-posting and Bottom-posting are, read the link below:
        <http://www.idallen.com/topposting.html>

        * Sending HTML formatted messages: If you don’t know why this is a terrible idea, see
        <http://archive.birdhouse.org/etc/evilmail.html>


    !!! Danger "Beware"
        If you use gmail, remember to at least use "Plain text mode" when replying.
        Mailing lists usually blocks formatted emails.
        And remember to do bottom post.

#### Subscribing to Mailing lists

Subscribe in the following mailing lists, click to see instructions:

??? note "kernelnewbies@kernelnewbies.org"
    Use this list to post questions about kernel development.

    Take a look at <a>https://kernelnewbies.org</a>

    Subscribe to the list through <a>http://lists.kernelnewbies.org/mailman/listinfo/kernelnewbies</a>

    Make sure you read the kernelnewbies mailing list guidelines before submitting any message.

    <a>https://kernelnewbies.org/mailinglistguidelines</a>


??? note "lkcamp@lists.libreplanetbr.org"
    Internal list for communication of this community.

    Use this list for questions which don’t fit the kernelnewbies guidelines,
    for questions specific to this group or for discussions about this document.

    Also, when submitting questions to kernelnewbies,
    add this mailing list in CC to request feedback from us.

    Subscribe to this list through  <a>https://lists.libreplanetbr.org/mailman/listinfo/lkcamp</a>

#### IRC Channels

Sometimes email is just too formal for simple questions. IRC offers a live chat (similar to hangouts, whatsapp or telegram).

??? note "What is IRC?"
    IRC is an open chat protocol, anyone can run an IRC server and provide several chat channels, which means that you need to know in advance which server you need to connect and which channels you want to join.

    An introduction tutorial on IRC usage can be found at:

    <a>https://fedoramagazine.org/beginners-guide-irc</a>

We will join the following channels:

??? note "<b>#kernelnewbies @ irc.oftc.net</b>"
    Use this channel for questions regarding kernel development (almos same rules from kernelnewbies mailing list guideline applies here. Don't be afraid to ask here, we will be monitoring this channel to help answering your questions.

??? note "<b>#lkcamp @ irc.freenode.net</b>"
    <p>
    Use this channel when the question is something internal to our group or if you think that it is not fit for the #kernelnewbies
    </p>

We will perform the follow 3 steps (click to expand):

??? note "Install an IRC client"
    Install any IRC client, there is a list here: <a>https://kernelnewbies.org/IRC</a>, we recommend hexchat for beginners.

    !!! note
        If you are not being able to connect due to some firewall,
        you can use a webclient.

        Example: <a>http://kiwiirc.com</a>

        - Click at try me -> add network;
        - then fill just the server and the nickname (leave the rest empty);
        - then click on “Connect” at the top of the page.
        - Add a network for both server <b>irc.freenode.net</b> and <b>irc.oftc.net</b>
        - join the channels <b>#lkcamp @ irc.freenode.net</b> and <b>#kernelnewbies @ irc.oftc.net</b>.

??? note "Connect to the IRC servers <b>irc.oftc.net</b> and join <b>#kernelnewbies</b>"
    To connect to the <b>irc.oftc.net</b> server, type in the irc client:

        ```
        /server irc.oftc.net
        ```

    Then join the kernelnewbies channel by typing:

        ```
        /join #kernelnewbies
        ```

??? note "Connect to the IRC servers <b>irc.freenode.net</b> and join <b>#lkcamp</b>"
    Now connect to the irc.freenode.server, type:

        ```
        /server irc.freenode.net
        ```

    Then join the channel:

        ```
        /join #lkcamp
        ```

    Then say hi in **#lkcamp** to complete this task ;)

    ???+ warning
        Before making questions remember that people are volunteers. Being polite and showing that you did some research on the topic beforehand will help you with getting the attention from experts, for example: "I found the documentation X, but I couldn't understand Y, could someone help me by explaining what the command Z means please?"

### (2) Compiling Kernel from source and running it in a virtual machine

!!! warning "If you are in IC labs running GNOME"
    Edit `~/.config/clipit/clipitrc` and change `history_timeout` to false
    `history_timeout = false`
    This will enable copy and paste in your window manager.

!!! requirements for this section
    - Bash
    - Python 3.3
    - Compiled kernel

When writting your own code, sometimes things goes bad and your entire system crashes,
leaving you no option but to reboot your machine. To avoid messing up with your system,
it is recommended to make tests in a virtual machine.

You can use `virtme` to achieve that.

!!! important
    If you used a virtual machine for the previous step because and
    you didn't have sudo powers on your host machine,
    you can follow the tutorial below directly on your host machine (not in the VM)
    because the following instructions doesn't require root priviledges.

#### Install virtme as a normal user

```bash
$ pip3 install --user git+https://github.com/ezequielgarcia/virtme.git
```

??? note "If `pip3` fails..."
    ```bash
    $ cd ~/
    $ git clone https://github.com/ezequielgarcia/virtme.git
    $ cd virtme
    $ ./setup.py install --user
    ```

#### Getting dependencies to compile Linux Kernel

??? note "For Ubuntu 16.04, Ubuntu 18.x, Debian Jessie or Debian Stretch"
    ```bash
    apt-get -y -q install          \
      bc                           \
      flex                         \
      bison                        \
      build-essential              \
      git                          \
      libncurses-dev               \
      libssl-dev                   \
      libelf-dev                   \
      u-boot-tools                 \
      wget                         \
      xz-utils                     \
      qemu-kvm                     \
      iproute2                     \
      python3                      \
      python3-pip
    ```

    The package list may be different on other Ubuntu versions, or other distributions.

??? note "For Arch Linux and derivatives (x86_64)"
    ```bash
    pacman -S base-devel git ncurses libelf uboot-tools qemu python-pip
    ```


#### Fetch the Linux Kernel repository

!!! note
    If you are physically attending the meetings at the University of Campinas,
    use the `/tmp` folder,
    otherwise you probably won't have enough quota space.

```bash
$ cd /tmp
$ git clone git://git.kernel.org/pub/scm/linux/kernel/git/gregkh/staging.git -b staging-testing --depth=1
$ cd staging
```

!!! Tip
    If the <b>git clone</b> is taking too long,
    and you are physically attending the meeting at IC,
    change the upstream URL to an IC mirror:

    ```bash
    $ git clone https://gitlab.ic.unicamp.br/lkcamp/linux-staging --depth=1
    $ cd linux-staging
    ```



#### Compile your Kernel with required modules for virtme

```bash
$ cd staging # the folder where you downloaded the kernel tree in step (2)
$ virtme-configkernel --defconfig
$ make -j$(nproc) # $(nproc) calls a command which returns an integer representing the number of cores in your machine. E.g: 8
```

!!! warning
    If your terminal cannot find the `virtme-configkernel`,
    and **you are in bash** and have at least `Python` **3.3** installed,
    chances are that your `$PATH` is lacking the default python user installment location.
    To fix that you can run:
    ```bash
    export PATH=${PATH}:${HOME}/.local/bin
    ```

    If this solves your problem,
    remember to add this command to the ~/.bashrc in order to execute it every time you run `bash`.
    ```bash
    echo 'export PATH=${PATH}:${HOME}/.local/bin' >> ~/.bashrc
    ```


#### Run your Kernel
```bash
$ virtme-run --kdir=.
```

!!! important "Quit the virtualized environment"
    Type `Ctrl-a` followed by `x`.

    !!! note
        If you are using a terminal emulator which binds Ctrl-A to other command,
        this method will not work.

#### Customize your Kernel

Follow the steps from the screenshots to change the kernel version suffix!
```bash
# Change to the kernel source root directory
$ cd /tmp/staging
$ make menuconfig
```

##### Steps
- Select General Setup
![Select General Setup](../img/mc_1.png)
- Enter into Local Version
![Enter into Local Version](../img/mc_2.png)
- Write down your kernel version suffix
![Write down your kernel version suffix](../img/mc_3.png)
![Write down your kernel version suffix](../img/mc_4.png)
- Check that your suffix has been inserted into Local Version entry, then use the arrow keys to navigate to the "Save" button and type `Return` to save your .config
![Type ESC multiple times to exit](../img/mc_5.png)
- Type `ESC` multiple times to exit

```bash
$ make -j$(nproc)
$ virtme-run --kdir=.
$ uname -r
```

#### Quit the virtualized environment
Type `Ctrl-a` followed by `x`.

### Done?

Congrats! You ran your own compiled kernel! \o/

Now go back to what you did and try to search more commands about your IRC client, the commands used to fetch the source code, the commands you typed, check the documentation and how it works. If you are completely done, ask on IRC if another person needs help before moving on in this guide.

Recommended reading: Chapter 1: An Introduction to Device Drivers of Linux Device Drivers 3rd, edition <https://lwn.net/Kernel/LDD3/> .

Don’t forget to update the spreadsheet for tracking our progress.

### (Extra) Booting the kernel on your own machine

??? note "Click here if you don't have sudo powers"

    <p>
    Since there is no sudo power, we need to run a virtual machine.
    You can just install a distro inside a virtual machine and proceed with the tutorial.
    But to save some time, we prepared an image for you already:
    </p>
    ???+ note
        If you are physically attending the meetings at the University of Campinas, use the /tmp folder, otherwise you
        won't have enough quota space.

    ```bash
    $ cd /tmp
    $ wget https://gitlab.com/lkcamp/binaries/raw/master/virtual_machine/faime-L4PAWK44.qcow2
    $ qemu-kvm -hda faime-L4PAWK44.qcow2 -m 4G -smp 4 -net nic -net user,hostfwd=tcp::5555-:22 # the password is debian
    ```

    !!! warning

        The root password of the virtual machine is `debian`

    <p>
    The `hostfwd` command is to allow you to access the virtual machine by ssh (to make it easier to copy & paste commands):
    </p>

    ```bash
    ssh -p 5555 debian@localhost # the password is debian
    ```

    <p>
    Now, proceed with the tutorial below as if the VM was your physical machine.
    </p>

??? note "Click here if you use Arch Linux"

    <p>
    <div class="admonition warning">
    <p class="admonition-title">Warning</p>
    <p>Some people had issues when trying to boot the custom kernel in Arch Linux in encrypted disks.</p>
    </div>
    Arch Linux users might have some problems following the steps described in the module.
    In summary to compile the kernel on Arch Linux you will need (assuming you have the kernel source and cd into that directory):

    ```bash
    $ zcat /proc/config.gz > .config  # get the .config of your current installed kernel
    $ make menuconfig                 # (or any alternative if you want to build a custom kernel)
    $ make -jN                        # where N is the number of parallel jobs
    $ make modules install
    ```

    NAME is the name you will give to your kernel
    ```bash
    $ cp -v arch/x86_64/boot/bzImage /boot/vmlinuz-NAME</code>
    ```

    The kernelversion will be the same as the name of the custom kernel source directory (and the modules directory for it, located in /usr/lib/modules/).
    ```bash
    $ mkinitcpio -k &lt;kernelversion&gt; -g /boot/initramfs-&lt;file name&gt;.img
    $ grub-mkconfig -o /boot/grub/grub.cfg
    ```

    For more information please check: https://wiki.archlinux.org/index.php/Kernel/Traditional_compilation
    </p>

!!! Warning
    To issue `sudo` commands with the *debian* user,
    you must transform it into a sudoer:
    ```bash
    su -c 'usermod -aG sudo debian'
    exit # Logoff
    ```
    And then logon again.

??? note "Next steps"
    Proceed with the tutorial in the kernelnewbies page below from session `Install some packages` to `Running your kernel`.

    <https://kernelnewbies.org/OutreachyfirstpatchSetup>

!!! note
    While you wait for the kernel to download and compile, start reading the book Linux Device Drivers at the link below:

<http://lwn.net/Kernel/LDD3/>

Don't be afraid to ask questions at #kernelnewbies or #lkcamp, we'll be there to help.

